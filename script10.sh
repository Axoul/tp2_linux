#!/bin/bash
echo -e "Fichiers les plus gros :\n"
find $HOME -type f -printf '%s %p\n' | sort -nr | head -5 #Liste les 5 plus gros fichier du dossier personnel
echo -e "\nLa corbeille contient $(ls $HOME/.local/share/Trash/files -1 | wc -l) ficher(s)" #Compte le nombre de fichiers dans la corbeille
