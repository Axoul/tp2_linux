#!/bin/bash
if [ ! $# -gt 1 ]; then #Si il n'y a pas de paramètres
  echo "Utiliser cette syntaxe : ./script8.sh nomdufichier1 nomdufichier2 ..."
else
  for param in "$@" #Pour chaque paramètre
  do
    echo $(file $param)
  done
fi
