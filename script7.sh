#!/bin/bash
if [ ! $# -gt 1 ]; then #Si il n'y a pas de paramètres
  echo "Boujour à tous !"
else
  flag=false #Booléen pour alterner bonjour et salut
  for param in "$@" #Pour chaque paramètre
  do
    if [ $flag = false ]; then #Si c'est un paramètre impair
      echo "Bonjour $param !"
      flag=true
    else
      echo "Salut $param !"
      flag=false
    fi
  done
fi
