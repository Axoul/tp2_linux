#!/bin/bash

if [ -z "$1" ]; then #Si le premier paramètre n'est pas présent
  echo "Utiliser cette syntaxe : ./archiveur.sh nomdudossier"
else
  if [ ! "$(ls -A $1)" ]; then #Si le dossier est vide
    echo "$1 est vide"
  else
    FILENAME=${1%?} #Enlève le / du nom de dossier pour en faire le nom de l'archive
    tar czf $FILENAME.tar.gz $1 #Compresse le dossier avec le paramètre Gzip et sans verbose
  fi
fi
