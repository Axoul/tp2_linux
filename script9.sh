#!/bin/bash
if [ -z "$1" ]; then #Si le premier paramètre n'est pas présent
  echo "Utiliser cette syntaxe : ./script9.sh nomdudossier"
else
  cpt=0 #Initialisation compteur
  for filename in $1*
  do
    file $filename #Information du fichier
    cpt=$((cpt+1)) #Incrémentation compteur
  done
  echo "Il y a $cpt fichier(s)"
fi
