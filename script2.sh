#!/bin/bash
# Exemple d'un script shell appelé script2.sh
# Ce script afche le contenu d'un message stocké dans une variable
nom=Julie
echo 1. C\'est $nom.
echo -n "On ne retourne pas a la ligne. "
echo "2. Et c'est encore $nom."
echo 3. C'est toujours $nom.
exit 0 
